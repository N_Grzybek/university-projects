---
title: "Data Mining - Cluster Analysis and Dimension Reduction"
output:
  pdf_document: 
    latex_engine: xelatex 
  word_document: default
fontsize: 11pt
geometry: margin=3cm
fig_caption: true
number_sections: true
---
```{r warning=FALSE, include=FALSE}

library(plyr)
library(corrplot)
library(ggplot2)
library(gridExtra)
library(ggthemes)
library(caret)
library(MASS)
library(randomForest)
library(party)
library(DataExplorer)
library(dplyr)
library(tidyverse)  # data manipulation
library(cluster)    # clustering algorithms
library(factoextra) # clustering visualization
library(dendextend) # for comparing two dendrograms
library(fpc)
library(stats)
library(janitor)
library(clusterSim)
library(clValid)
library(mclust) # required for model-based clustering
library(ggpubr)
```

\renewcommand*\contentsname{​​​​​Table of contects}​​​​​
\tableofcontents 


\newpage

# 1. Preparing the dataset

For this part of the project, we used the cleaned telco customer service data obtained from the first part of the project. This means that they do not have nan values and that they do not contain the Total Charges column, whose reason for removal we also explained in the first part. Due to long calculation time we randomly selected 500 observation from our dataset. After that, the dataset has 500 rows and 19 columns. 


```{r include=FALSE, cache = TRUE}
churn <- read.csv("data1.csv", stringsAsFactors = TRUE)
str(churn)
```


```{r include=FALSE, cache = TRUE}
head(churn)
```



```{r include=FALSE, cache = TRUE}
#change the size of the data set due to time consumption
df <- churn[sample(nrow(churn), size = 500),] 
```

```{r include=FALSE, cache = TRUE}
dim(df)
```


```{r include=FALSE, cache = TRUE}
churn.real.labels <- df$Churn
```



```{r include=FALSE, cache = TRUE}
n <- seq(1, length(df$Churn), by = 1)
```



```{r include=FALSE, cache = TRUE}
df$Churn <- paste(df$Churn, ave(n, n, n), sep='.')

```


```{r include=FALSE, cache = TRUE}
rownames(df) <- df$Churn

```


The clustering was carried out base on the column Churn, which tells whether the customer uses the company's service or not. As no input for clustering algorithm we use data from 5 columns. There are "gender", "SeniorCitizen", "tenure", "PhoneService", "MonthlyCharges". We want to see how much data from each category is in a single cluster.  


```{r include=FALSE, cache = TRUE}
#remove the Churn and other unuseful rows
selectedFeatures <- setdiff(colnames(df),c("Partner",  "Dependents", "MultipleLines", "Contract", "PaymentMethod","InternetService","OnlineSecurity","OnlineBackup", "DeviceProtection", "TechSupport", "StreamingTV", "StreamingMovies", "PaperlessBilling", "PaymentsMethod","Churn"))
selectedFeatures
```

```{r include=FALSE, cache = TRUE}
df.selected <- df[, selectedFeatures]
df.selected 

```


To simplify further calculations, the columns containing categorical data have been transformed accordingly: 

* Column "gender": we replaced the value "male" with the number 1 and "female" with the number 0.
* Column "SeniorCitizen", "PhoneService": we replaced the value "Yes" to 1 and "No" to 0.

Two other columns "tenure" and "MonthlyCharges" are numeric. 


```{r include=FALSE, cache = TRUE}
#change the Male for 1 and Female for 0
df.selected$gender <- ifelse(df.selected$gender == "Male", 1,0)
```

```{r include=FALSE, cache = TRUE}
#change the Yes for 1 and No for 0
df.selected$SeniorCitizen <- ifelse(df.selected$SeniorCitizen == "Yes", 1,0)
df.selected$PhoneService <- ifelse(df.selected$PhoneService == "Yes", 1,0)
```

```{r include=FALSE, cache = TRUE}
df.selected
```



# 2.Deterimining optimal number of clusters

The next step was determining optimal number of cluster. We used well-known "elbow" method and silhouette method. 

"Elbow" method is an empirical method to find the optimal number of clusters for a dataset. In this method we pick a range of candidate values of k and then apply k-means clustering using each of the values of k. The next step is to find the average distance of each point in a cluster to its centroid and represent it in a plot. With the increase in the number of clusters (k), the average distance decreases, so to find the optimal number of clusters (k), we have to observe the plot and find the value of k where there is a sharp and steep fall of the distance. 

The silhouette method computes silhouette coefficient of each point that measure how much a point is similar to its own cluster compared to other clusters. After computing the silhouette coefficient for each point, average it out, to get the silhouette score.  The output of this method in R is a graphical representation of how well each object has been classified.  


Unfortunately, using the "elbow" method we do not see an exact value for the optimal number of clusters and for many calls, the graphical silhouette coefficient indicates different values, but we decided to devide our data into 2 clusters. 




```{r echo=FALSE, message=FALSE, warning=FALSE, cache = TRUE}
fviz_nbclust(df.selected, FUNcluster = kmeans, method="wss", k.max=10)
```


```{r echo=FALSE, cache = TRUE}
set.seed(74)
fviz_nbclust(df.selected, FUNcluster = hcut, method = "silhouette")
```


To ensure that the optimum number of clusters is 2, we can visualize silhouette information from clustering. We see that for k-means algorithms 2 clusters fit well. All of the clusters' values are above average silhouette score. There are no negative numbers, which means that all of the points are well classified.  

Good results we get agnes algorithm. There are no clusters with negative values, and still clusters' values are above average silhouette score. 


```{r echo=FALSE, message=FALSE, warning=FALSE, cache = TRUE}
#### Visualize silhouette information from clustering
# k-menas clustering
km.res <- kmeans(df.selected, 2, nstart=10)
sil.kmeans <- silhouette(km.res$cluster, dist(df.selected))


# AGNES (hierarchical clsutering)

agnes.res <- agnes(df.selected, method="ward")
agnes.partition <- cutree(agnes.res, k=2)
sil.agnes <- silhouette(agnes.partition, dist(df.selected))
par(mfrow = c(1, 2))
fviz_silhouette(sil.kmeans, xlab="K-means")
fviz_silhouette(sil.agnes, xlab="AGNES")
```


The plots of the connectivity, Dunn Index and Silhouette Width are showed in the below figure. Recall that the connectivity should be minimized, while both the Dunn Index and Silhouette Width should be maximize. Thus, for hierarchical clustering the optimal number of clusters is 2 but for pam and k-means it can be 2.


```{r echo=FALSE, message=FALSE, warning=FALSE, cache = TRUE}
# clustering algorithms
set.seed(74)
methods <- c("hierarchical","kmeans", "diana", "pam")

# range for number of clusters
K.range <- 2:6

internal.validation <- clValid(df.selected, nClust=K.range, clMethods=methods, validation="internal")

summary(internal.validation)
optimalScores(internal.validation)

par(mfrow = c(2, 2))
plot(internal.validation, legend = FALSE, lwd=2)
plot.new()
legend("center", clusterMethods(internal.validation), col=1:9, lty=1:9, pch=paste(1:9))
```



# 3. Partitioning methods

## 3.1 PAM algorithm 

Pam algorithm partitions the dataset of n objects into k clasters, where the dataset and k (number of clusters) are the input of the algorithm. This algorithm works with a matrix of dissimilarity, whose goal is to minimize the dissimilarity between the represents of each cluster and its members. 


### 3.1.1 Clustering

First we created the dissimilarity matrix and then use the pam algorithm. The inputs of this algorithms were the dissimilarity matrix and k=2. 

```{r include=FALSE, cache = TRUE}
set.seed(74)
df_data.DissimilarityMatrix <- daisy(df.selected)
```

```{r include=FALSE, cache = TRUE}
df_data.DissimilarityMatrix.mat <- as.matrix(df_data.DissimilarityMatrix)
```

```{r include=FALSE, cache = TRUE}
#PAM algorithm
set.seed(74)
df.pam3 <- pam(x=df_data.DissimilarityMatrix.mat, diss=TRUE, k=2)
```


```{r include=FALSE, cache = TRUE}
df.selected$cluster_pam <- df.pam3$cluster
```




```{r include=FALSE, cache = TRUE}
cluster.labels <- df.pam3$clustering
```

In the plot we showed how the PAM algorithm works for our dataset. In the x axis there is tenure and in the y axis there are monthly charges. We see that algorithm has distinguished 2 groups. We want to know how many customers still use the service of the company and how many does not. 


```{r echo=FALSE, cache = TRUE}
plot(df.selected$tenure, df.selected$MonthlyCharges, col=cluster.labels, xlab="Tenure", ylab="Monthly Charges")
title("Visualization of cluster analysis results for PAM Algorythm")

```

This numbers are showed in the below table:


```{r echo=FALSE, cache = TRUE}
tab <- table(cluster.labels, churn.real.labels)
tab
```


We see that in all clusters there are more customers who no longer use the company's services.

### 3.1.2 What variables are in the clasters ? 


We will now check what the remaining values are in each cluster. First we will check how many women and men are in each cluster. Recall that 1 stands for male and 0 for female.



```{r echo=FALSE, cache = TRUE}

tabyl(df.selected, gender, cluster_pam)
```

Now we want to check how many young and senior citizen was in each clusters. Recall that 1 means that person is senior and 0 means that the person is young.

We see that in each cluster there are more young customers.  


```{r echo=FALSE, cache = TRUE}

tabyl(df.selected, SeniorCitizen, cluster_pam)
```

We will now look at how many customers use the phone service. Recall that 1 means "Yes" and 0 means "No". In each cluster, most people used the phone service.

We see that there are more people who use the phone service.   


```{r echo=FALSE, cache = TRUE}

tabyl(df.selected, PhoneService, cluster_pam)
```




```{r include=FALSE, cache = TRUE}
ClusterCenters.names <- df.pam3$medoids
ClusterCenters.names
```


## 3.2 K-means algorythm

K-means algorithm attempts to discover similarities within the dataset by grouping objects such that objects in the same cluster are more similar to each other than they are to objects in another cluster.  K-means groups similar data points together into clusters by minimizing the mean distance between geometric points. It iteratively partitions datasets into a fixed number (k) of non-overlapping subgroups where in each data points belongs to the cluster with the nearest mean cluster center.

### 3.2.1 Clustering 

We used the kmeans function in R to make clustering, the input arguments were our selected data and number of clusters. We set number of clusters to 2. 



```{r include=FALSE, cache = TRUE}
k <- 2
set.seed(74)
kmeans.k4 <- kmeans(df.selected, centers = k, iter.max = 10, nstart = 10)

```


The result of the algorithm and clustering can be seen in the figure below. We see a division into 2 clusters, but also many outliers observations.  


```{r echo=FALSE, message=FALSE, warning=FALSE, cache = TRUE}
#visualization
fviz_cluster(kmeans.k4, df.selected, ellipse.type="euclid", repel = TRUE)

```

```{r include=FALSE, cache = TRUE}
df.selected$cluster_kmeans <- kmeans.k4$cluster
```

Let's check how many observations there are in each cluster. The population of each cluster is shown below.


```{r echo=FALSE, cache = TRUE}
table(df.selected$cluster_kmeans)
```
### 3.2.2 What variables are in the clusters? 

The results of how many women and men are in each cluster are shown in the table below. 


```{r echo=FALSE, cache = TRUE}
tabyl(df.selected, gender, cluster_kmeans)
```

As before, there are more young customers in each cluster. 


```{r echo=FALSE, cache = TRUE}

tabyl(df.selected, SeniorCitizen, cluster_kmeans)
```

As before, in each cluster there are more customers who have used the phone service. 


```{r echo=FALSE, cache = TRUE}

tabyl(df.selected, PhoneService, cluster_kmeans)
```



# 4.Hierarchical methods

## 4.1 AGNES algorithm

Agglomerative clustering works in a "bottom-up" manner. That is, each object is initially considered as a single element cluster. At each step of the algorithm, two clusters that are the most similar are combined into a new bigger cluster. This process is iterated until all points are member of just one single big cluster (root). 

First we have to find the best linkage method which will be used for computing distance between clusters. We checked 4 methods:

* average (The distance between two clusters is defined as the average distance between the elements in cluster 1 and the elements in cluster 2)
* complete (the distance between two clusters is defined as the maximum value of all pairwise distances between the elements in cluster 1 and the elements in             cluster 2)
* single (The distance between two clusters is defined as the minimum value of all pairwise distances between the elements in cluster 1 and the elements in             cluster 2)
* ward (Ward’s minimum variance method: It minimizes the total within-cluster variance. At each step the pair of clusters with minimum between-cluster              distance are merged.)

In order to choose the best method, we check the agglomerative coefficient which measures the amount of clustering structure found. Values closer to 1 suggest strong clustering structure.

```{r include=FALSE, cache = TRUE}
set.seed(74)
df.selected.agnes.avg <- agnes(x = df_data.DissimilarityMatrix.mat, diss = TRUE, method = "average")
df.selected.agnes.complete <- agnes(x = df_data.DissimilarityMatrix.mat, diss = TRUE, method = "complete")
df.selected.agnes.single <- agnes(x = df_data.DissimilarityMatrix.mat, diss = TRUE, method = "single")
df.selected.agnes.ward <- agnes(x = df_data.DissimilarityMatrix.mat, diss = TRUE, method = "ward")
```


In the below figure we see that three methods we have high agglomerative coefficient. There are average, complete and ward linkages. Among this three methods, the Ward method proved to be the most effective.  

### 4.1.1 Compering methods

```{r echo=FALSE, message=FALSE, warning=FALSE, cache = TRUE}
par(mfrow = c(2, 2))

par(cex=0.6)
plot(df.selected.agnes.avg, which.plots=2,main="AGNES: average linkage")


par(cex=0.6)
plot(df.selected.agnes.complete, which.plots=2,main="AGNES: complete linkage")

par(cex=0.6)
plot(df.selected.agnes.single, which.plots=2,main="AGNES: single linkage")

par(cex=0.6)
plot(df.selected.agnes.ward, which.plots=2,main="AGNES: ward linkage")


```



### 4.1.2 Dendograms 

The dendograms obtained using the agnes algorithm with the ward method, divided into 2 clusters are shown in the following two figures. 


```{r echo=FALSE, message=FALSE, warning=FALSE, cache = TRUE}
fviz_dend(df.selected.agnes.ward, k=2, cex=0.6, main = "Dendogram - ward linkage")

```

```{r echo=FALSE, message=FALSE, warning=FALSE, cache = TRUE}
# circular dendrogram
fviz_dend(df.selected.agnes.ward, type="circular", cex=0.6, k=2,  main="Circular dendrogram - ward linkage")
```



### 4.1.3 What variables are in the clusters? 


```{r include=FALSE, cache = TRUE}
df.selected.agnes.ward.k2 <- cutree(df.selected.agnes.ward, k=2)  # 2 clusters
```


```{r include=FALSE, cache = TRUE}
df.selected$cluster_agnes <- df.selected.agnes.ward.k2
```

```{r include=FALSE, cache = TRUE}
df.selected
```

Let's find out how numerous each cluster is. 


```{r echo=FALSE, message=FALSE, warning=FALSE, cache = TRUE}
# Cluster size 

table(df.selected.agnes.ward.k2)

```


```{r echo=FALSE, message=FALSE, warning=FALSE, cache = TRUE}
tabyl(df.selected, gender, cluster_agnes)
```

As for the previous two algorithms, young people predominate in each cluster.


```{r echo=FALSE, message=FALSE, warning=FALSE, cache = TRUE}

tabyl(df.selected, SeniorCitizen, cluster_agnes)
```

Each majority of customers used the phone service.  



```{r echo=FALSE, message=FALSE, warning=FALSE, cache = TRUE}

tabyl(df.selected, PhoneService, cluster_agnes)
```


## 4.2 DIANA algorythm

DIANA algorithm works in a "top-down" manner. It begins with the root in which all objects are included in a single cluster. At each step of iteration, the most heterogeneous cluster is divided into two. The process is iterated until all objects are in their own cluster.



```{r include=FALSE, cache = TRUE}
set.seed(74)
df.selected.diana <- diana(x = df_data.DissimilarityMatrix.mat, diss = TRUE)
```


### 4.2.1 Dendograms

The dendograms obtained using the diana algorithm, divided into 2 clusters are shown in the following two figures.

```{r echo=FALSE, message=FALSE, warning=FALSE, cache = TRUE}
fviz_dend(df.selected.diana, k=2, cex=0.6, main = "DIANA dendogram")

```

```{r echo=FALSE, message=FALSE, warning=FALSE, cache = TRUE}
# circular dendrogram
fviz_dend(df.selected.diana, type="circular", cex=0.6, k=2,  main="DIANA circular dendrogram")
```

### 4.1.3 What variables are in the clusters? 


```{r include=FALSE, cache = TRUE}
df.selected.diana.k2 <- cutree(df.selected.diana, k=2)  # 4 clusters
```


```{r include=FALSE, cache = TRUE}
df.selected$cluster_diana <- df.selected.diana.k2
```

The size of the clusters is shown in the table below. It can be seen that cluster two is bigger than one, it is the same situations as in the previous algorithms.


```{r echo=FALSE, message=FALSE, warning=FALSE, cache = TRUE}
# Cluster size 

table(df.selected.diana.k2)

```

Below we see a table showing how many women and men are in each cluster. 


```{r echo=FALSE, message=FALSE, warning=FALSE, cache = TRUE}
tabyl(df.selected, gender, cluster_diana)
```


As for the previous algorithms, young people predominate in the clusters. 

```{r echo=FALSE, message=FALSE, warning=FALSE, cache = TRUE}

tabyl(df.selected, SeniorCitizen, cluster_diana)
```

Also, the majority of people in the clusters have used phone service.


```{r echo=FALSE, message=FALSE, warning=FALSE, cache = TRUE}

tabyl(df.selected, PhoneService, cluster_diana)
```

 




# 5. Dimensional reduction

## 5.1. Prinicpal Component Analysis (PCA)


The main idea about the PCA is reduce a large data set into a smaller one while maintaining most of its variation information. PCA is an unsupervised machine learning technique that seeks to find principal components, linear combinations of the original predictors.   


We use the R built-in function prcomp() to calculate the principal components of the dataset. We specify the scale = TRUE so that each of the variables in the dataset are scaled to have a mean of 0 and standard deviation of 1 before calculating the principal components. We set the parameter center as TRUE too. Thanks to this we performed the standarization of or data. 

It is important to note that eigenvectors in R point in the negative direction be default, so we multiplied by -1 to reverse the signs.

```{r echo=FALSE, cache=TRUE}
df.selected.old <- df.selected[c("gender", "SeniorCitizen", "tenure", "PhoneService", "MonthlyCharges")]
pca_result <- prcomp(df.selected.old, retx = TRUE,center = TRUE ,scale=TRUE) #data after pca 
pca_result$rotation <- -1*pca_result$rotation
pca_result$rotation
```

We see that the first principal component (PC1) has high values for MonthlyCharges which indicates that this principal component describes the most variation in this variable. 

We can also see that the second principal components (PC2) has a high value for SeniorCitizen, which indicates that this principle component places most of its emphasis on senior citizens. 


Next we create a bitplot. This plot projects each of the observations in the dataset onto a scatterplot that uses the first and second principal components as the axes. We set the scale parameter to 0 to ensure that the arrows in the plot are scaled to represent the loadings. 

```{r echo=FALSE, message=FALSE, warning=FALSE, cache=FALSE}
#visualization

fviz_pca_var(pca_result)
```

Now we calculate the total variance in the dataset explained by each principal components.


```{r message=FALSE, warning=FALSE, cache=FALSE, include=FALSE}
# Plot the explained variance
variance <- (pca_result$sdev ^2)/sum(pca_result$sdev^2)
cumulative.variance <- cumsum(variance)
par(mfrow = c(1, 2))
barplot(variance, main = "Total Variance")
par()
barplot(cumulative.variance, main = "Comulative Variance")

```

```{r echo=FALSE,cache = TRUE,out.width = "70%"}
fviz_eig(pca_result, addlabels = TRUE)
```

From the results we can observe the following: 

* The first principal component explained almost 30% of the total variance in the dataset
* Each of the second, third and fourth principal components explained 20% of the total variance in the dataset
* The fifth principal component explained 11% of the total variance in the dataset






```{r include=FALSE, cache = TRUE}
(variables <- get_pca_var(pca_result))
```


Now we check the correlation between the variables and principal components. We see that most variables are positively correlated with principal components. The strongest positive correlation is between MonthlyCharges and Dim1. The strongest negative correlation is between gender and Dim3. 


```{r echo=FALSE, message=FALSE, warning=FALSE, cache=TRUE, out.width="70%"}
# correlation of  variables and principal components (PCs)
par(mfrow = c(1, 1))

par(cex=0.6)
corrplot(variables$cor)

```


Now we check the contributions of variables using the fviz_contrib(). This function creates a barplot of contributions and a dashed line which corresponds to the expected value if the contribution where uniform. 


```{r echo=FALSE, message=FALSE, warning=FALSE, cache=TRUE, out.width="70%"}
# contribution of variables to Dim-1
fviz_contrib(pca_result, which.plots=2,choice="var", axes=1)

```

```{r echo=FALSE, message=FALSE, warning=FALSE, cache=TRUE, out.width="70%"}
# contribution of variables to Dim-2

fviz_contrib(pca_result, which.plots=2, choice="var", axes=2)

```

```{r echo=FALSE, message=FALSE, warning=FALSE, cache=TRUE, out.width="70%"}
# contribution of variables to Dim-3

fviz_contrib(pca_result, which.plots=2, choice="var", axes=3)

```

```{r echo=FALSE, message=FALSE, warning=FALSE, cache=TRUE, out.width="70%"}
# contribution of variables to Dim-4

fviz_contrib(pca_result, which.plots=2, choice="var", axes=4)

```

```{r echo=FALSE, message=FALSE, warning=FALSE, cache=TRUE, out.width="70%"}
#contribution of variables to Dim-5

fviz_contrib(pca_result, choice="var", axes=5)
```





## 5.2. Multidimensional Scaling (MDS)

Multidimensional scaling (MDS) is a popular approach for graphically representing relationships between objects in multidimensional space. Dimension reduction via MDS is achieved by taking the original set of samples and calculating a dissimilarity (distance) measure for each pairwise comparison of samples. The samples are then usually represented graphically in two dimensions such that the distance between points on the plot approximates their multivariate dissimilarity as closely as possible.



```{r cache=FALSE, include=FALSE, cache = TRUE}
# Derive dissimilarities between objects
dissimilarities <- daisy(df.selected.old, stand=TRUE)
dis.matrix <- as.matrix(dissimilarities)

mds.result <- cmdscale(dis.matrix, k=2)

```

Data after Multidimensional Scaling


```{r echo=FALSE, message=FALSE, warning=FALSE, cache = TRUE, out.width="70%"}
plot(mds.result[,1], mds.result[,2], pch=16)
```


Now lets make some interpretation of MDS plot. MDS arranges the points on the plot so that the distances among each pair of points correlates as best as possible to the dissimilarity between those two samples. The objects that are closer together on the plot are more alike than those further apart. 


## 5.3. Stress criterion and Shepard diagram 



```{r include=FALSE, cache = TRUE}
dist.mds.k2 <- dist(mds.result, method="euclidean") # we compute Euclidean distances in the new space
dist.mds.k2 <- as.matrix(dist.mds.k2)
```


```{r cache=FALSE, include=FALSE}
dis.original <- dis.matrix
STRESS <- sum((dis.original-dist.mds.k2)^2)
```


```{r echo=FALSE, message=FALSE, warning=FALSE, cache=TRUE, out.width="70%"}
plot(dis.original,dist.mds.k2, main="Shepard diagram", cex=0.5, xlab="original distance", ylab="distance after MDS mapping")
abline(coef=c(0,1), col="red", lty=2) # diagonal
```


Shepard diagram provides a quick and visual way to assess the quality of the MDS solution and to identify potential problems with the analysis. If the Shepard diagram shows a strong linear relationship, symmetry, and homoscedasticity, it is a good indication that the MDS solution is a good representation of the original dissimilarities in the data.

As we can see in the Shepard diagram above for 2 dimensions, the relationship is not so linear, which means that the MDS solution here is not good representation of the original dissimilarities and which indicates that it has a high stress vs dimension value therefore to find the best reduced dimension number we have the plot below which will show us the relationship between stress and MDS dimensions.


```{r include=FALSE, cache = TRUE}
d.max <- 9
stress.vec <- numeric(d.max)

par(mfrow=c(3,3))

for (d in 1:d.max)
{
    mds.k <- cmdscale(dis.matrix, k = d)
    dist.mds.k <- dist(mds.k, method="euclidean") # we compute Euclidean distances in the new space
    dis.original <- dis.matrix
    dist.mds.k <- as.matrix(dist.mds.k)
    STRESS <- sum((dis.original-dist.mds.k)^2)

    stress.vec[d] <- STRESS

    # Shepard diagram
    plot(dis.original,dist.mds.k, main=paste0("Shepard diagram (d=",d,")"),
         cex=0.5, xlab="original distance",  ylab="distance after MDS mapping")
    abline(coef=c(0,1), col="red", lty=2)
    grid()
    legend(x="topleft",legend=paste("STRESS = ",signif(STRESS,3)), bg="azure2")
}
```



```{r echo=FALSE, message=FALSE, warning=FALSE, cache=TRUE, out.width="70%"}

par(mfrow=c(1,1))

plot(1:d.max, stress.vec, lwd=2, type="b", pch=19, xlab="dimension (d)", ylab="STRESS")
title("STRESS vs. dimension")
grid()

```


As we can see, 4 dimensions might be quite good to use for our data, but the optimal number of dimensions is 5, which is the same number of the features we have in our data, which makes this MDS method not so effective for dimensional reduction compared to PCA which reduced the dimensions to two.

# 6. PCA for k-means alhorythm 

After performing PCA we checked how the dimension reduction influence k-means clustering.  

```{r echo=FALSE, message=FALSE, warning=FALSE, cache=FALSE}
# Coordinates of individuals
ind.coord <- as.data.frame(get_pca_ind(pca_result)$coord)
# Add clusters obtained using the K-means algorithm
ind.coord$cluster <- factor(kmeans.k4$cluster)
# Data inspection
head(ind.coord)
```

```{r message=FALSE, warning=FALSE, include=FALSE, cache = TRUE}
# Percentage of variance explained by dimensions
eigenvalue <- round(get_eigenvalue(pca_result), 1)
variance.percent <- eigenvalue$variance.percent
head(eigenvalue)
```

By conducting a PCA, we can see that we now have two large clusters that bring all the observations together. There are no values that occur outside the designated clusters.  

```{r echo=FALSE, message=FALSE, warning=FALSE, cache = TRUE,out.width="70%"}
ggscatter(
  ind.coord, x = "Dim.1", y = "Dim.2", 
  color = "cluster", palette = "npg", ellipse = TRUE, ellipse.type = "convex",
  legend = "right", ggtheme = theme_bw(),
  xlab = paste0("Dim 1 (", variance.percent[1], "% )" ),
  ylab = paste0("Dim 2 (", variance.percent[2], "% )" )
) +
  stat_mean(aes(color = cluster), size = 4)
```


# 7.Conclusion

Choosing the number of clusters was not an easy task, as the indicators we tested did not clearly indicate the optimal number of groups into which we should divide our data. However, we decided to divide the data into 2 clusters because we have two types of customers, those who use the company's services and those who do not, i.e. loyal customers and disloyal customers. We used different algorithms to compare how clusters form. These were partitioning (PAM and K-means) and hierarchical (AGNES and DIANA) methods. Cluster sizes vary depending on which type of algorithm we used. However, for all algorithms the clients in each cluster are mostly young people who have used the phone service. 
We also carried out a dimensionality reduction to see if we could get better results with this. Initially, we used the PCA method, which showed that the first three principal components explain a majority of the total variance in data. We also tested the MDS (Multidimensional Scaling) method, which proved to be inferior to the PCA method for our data. Finally, we examined how the PCA method affected the clustering of our data using the k-means algorithm. The results turned out to be better. In the graph, we do not see data that are outside the designated clusters.  
